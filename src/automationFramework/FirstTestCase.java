package automationFramework;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstTestCase {

    /**
     * @param args
     * @throws InterruptedException
     * @throws IOException 
     */
    public static void main(String[] args) throws InterruptedException, IOException {
        // Telling the system where to find the Chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\fmarcelo\\Downloads\\selenium\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();

        // Open google.com
        webDriver.navigate().to("http://www.store.demoqa.com");
        String CurrentUrl = webDriver.getCurrentUrl();
        String html = webDriver.getPageSource();

        // Printing result here.
        System.out.println(CurrentUrl);

        webDriver.close();
        webDriver.quit();
    }
}